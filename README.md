# Bastion host for AWS

## Configure Module 
```
module "bastion-host" {
    instance_type = ""
    image_ami = ""
    name = ""
    vpc_id = ""
}
```

### Import module

```
terraform init
```

## Give a star :stars:

## Want to improve or fix something? Fork me and send a MR! :punch: