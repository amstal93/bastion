
variable "instance_type" {
  default = "t3a.medium"
}

variable "image_ami" {
  default = "ami-04b9e92b5572fa0d1"
}

variable "name" {
  description = "Application name"
  type = string
}

variable "vpc_id" {
  description = "Vpc id to setup bastion"
  type        = string
}

data "aws_vpc" "selected" {
  id = var.vpc_id
}

data "aws_subnet_ids" "public" {
  vpc_id = data.aws_vpc.selected.id

  tags = {
    Tier = "Public"
  }
}

data "aws_subnet" "public" {
  for_each = data.aws_subnet_ids.public.ids
  id       = each.value
}